package mohitsharma9191.github.com.birthdayqr;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    ImageView mainPic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainPic = findViewById(R.id.mainPic);

        mainPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent scannerIntent = new Intent(getApplicationContext(), scanner.class);
                startActivity(scannerIntent);
            }
        });
    }
}
