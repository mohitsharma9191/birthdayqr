package mohitsharma9191.github.com.birthdayqr;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.zxing.ResultPoint;
import com.google.zxing.integration.android.IntentIntegrator;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import java.util.List;

public class scanner extends AppCompatActivity {

    private static final int MY_CAMERA_PERMISSION_REQUEST_CODE = 255;
    private static boolean isQRValid = false;

    private DecoratedBarcodeView mBarcodeView;

    private final BarcodeCallback mBarcodeCallback = new BarcodeCallback() {

        @Override
        public void barcodeResult(BarcodeResult result) {
            //TODO: Patient QR code validation needs to be added.
            isQRValid = true;

            navigateToResultScreen(result.getText());
        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {
            //Not used.
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);

        mBarcodeView = findViewById(R.id.zxing_barcode_scanner);
        initQRCaptureManager(savedInstanceState);

        ObserveQRCodeData();
    }

    private void navigateToResultScreen(String result){
        Intent resultIntent = new Intent(this, ScannerResult.class);
        resultIntent.putExtra("scanResult", result);
        startActivity(resultIntent);
    }

    /**
     * Method initialises the components required for qr code scanning
     *
     */
    public void initQRCaptureManager(Bundle savedInstanceState) {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setCaptureActivity(this.getClass());
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);

        CaptureManager captureManager = new CaptureManager(this, mBarcodeView);
        captureManager.initializeFromIntent(integrator.createScanIntent(), savedInstanceState);
        captureManager.decode();
        mBarcodeView.decodeContinuous(mBarcodeCallback);
    }

    /**
     * Method observes to boolean live data to show respective UI
     */
    private void ObserveQRCodeData() {
        if (!isQRValid) {
            startQRScan();
        } else {
            mBarcodeView.pause();
        }
    }

    /**
     * Starts the camera preview for QR code scanning
     */
    private void startQRScan() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_GRANTED) {
            mBarcodeView.resume();
        } else {
            requestPermissions(new String[]{Manifest.permission.CAMERA},
                    MY_CAMERA_PERMISSION_REQUEST_CODE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startQRScan();
            } else {
                Toast.makeText(this, "Camera Permission denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        startQRScan();
    }

    @Override
    public void onPause() {
        super.onPause();
        mBarcodeView.pause();
    }
}
