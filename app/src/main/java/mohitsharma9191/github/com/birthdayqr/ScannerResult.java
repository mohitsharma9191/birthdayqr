package mohitsharma9191.github.com.birthdayqr;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

public class ScannerResult extends AppCompatActivity {

    TextView resultTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner_result);

        resultTextView = findViewById(R.id.result_text);
        resultTextView.setMovementMethod(new ScrollingMovementMethod());
        resultTextView.setText(getIntent().getStringExtra("scanResult"));
    }
}
